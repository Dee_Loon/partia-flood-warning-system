"""This module contains a collection of functions related to
geographical data.
"""

from .utils import sorted_by_key
"""import haversine function to calculate distance between coordinates"""
from .haversine import haversine
import collections


def stations_by_distance (stations, Coordinate_P):
    """Make an empty list to store tuples"""
    ListToReturn = []
    """ Fill the list with name of station and distance"""
    for n in range(len(stations)):
        """ stations[n] returns the list of MonitoringClass objects in "stations" """
        """haversine() calculates distance of each station from coordinate P"""
        """stations[n].coord obtains coordinate of station"""
        set = (stations[n], haversine (stations[n].coord, Coordinate_P))
        """ adds the new tuple to the master list"""
        ListToReturn.append(set)
        pass
    
    """Sorts the list by distance using sorted_by_key"""
    return sorted_by_key(ListToReturn, 1)

def stations_within_radius(stations, centre, r):
    ListOfStationsNearCentre = []
    for n in range(len(stations)):
        """haversine() calculates distance of each station from coordinate P"""
        distance = haversine (stations[n].coord, centre)
        if distance <= r:
            ListOfStationsNearCentre.append(stations[n].name)
        else:
            pass
        
    return ListOfStationsNearCentre

def rivers_with_station(stations):
    return {station.river for station in stations}    
    
def stations_by_rivers(stations):
    dictionary = {}
    for station in stations:
        newlist = dictionary.setdefault(station.river, [])
        newlist.append(station.name) 
    return dictionary
    
def rivers_by_station_number(stations, N):
    river_list = []
    for station in stations:
        river_list.append(station.river)
   
    """
    Create a dictionary with frequency as the value and rivers as the key, 
    Using counter function.
    
    """ 
    
    d = {}
    d = collections.Counter(river_list)
    
    "Convert dictionary to sorted tuples"

    dtuple = d.items()
    dlist = sorted_by_key (dtuple, 1, reverse = True)
    
    return dlist[:N]

"""
    return collections.Counter(river_list).most_common(N)
"""


    