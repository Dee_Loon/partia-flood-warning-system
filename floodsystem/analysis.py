"""computes a least-squares fit of polynomial of degree p to water level data
given the water level time history (dates, levels) for a station"""

import numpy as np
import matplotlib

"code needs to accomodate unavailable data"

def polyfit(dates, levels, p):
    
    x = matplotlib.dates.date2num(dates)
    y = levels
    
    #shift axis
    d0 = x-x[0]

    # Find coefficients of best-fit polynomial f(x) of degree p
    p_coeff = np.polyfit(d0, y, p)
    # Convert coefficient into a polynomial that can be evaluated,
    # e.g. poly(0.3)
    poly = np.poly1d(p_coeff)
    
    
    
    return poly(d0), d0