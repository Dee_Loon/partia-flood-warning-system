import matplotlib.pyplot as plt
from datetime import datetime, timedelta

"""displays a plot of the water level data against time for a station"""
def plot_water_levels (stations, dates, levels):
    # Plot
    plt.plot(dates, levels)
    #Plot typical low and high water level
    plt.axhline(stations.typical_range[0], linewidth=2, color='r')
    plt.axhline(stations.typical_range[1], linewidth=2, color='r')
    
    # Add axis labels, rotate date labels and add plot title
    plt.xlabel('date')
    plt.ylabel('Water Level (m)')
    plt.xticks(rotation=45);
    #not sure correct need to test
    plt.title(stations.name)

    # Display plot
    plt.tight_layout()  # This makes sure plot does not cut off date labels
    plt.show()
    
    

#code might need to change depending on what kind of data i input into "stations"
"""plots the water level data and the best-fit polynomial"""
def plot_water_level_with_fit(station, dates, levels, p):
    # Plot
    plt.plot(dates, levels)
    
    # Add axis labels, rotate date labels and add plot title
    plt.xlabel('date')
    plt.ylabel('Water Level (m)')
    plt.xticks(rotation=45);
    #not sure correct need to test
    plt.title(station.name)

    # Display plot
    plt.tight_layout()  # This makes sure plot does not cut off date labels
    # Plot original data points
    plt.plot(dates, levels, '.')
    #Plot polynomial
    plt.plot(dates, p[0], color='b')