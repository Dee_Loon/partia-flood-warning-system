# -*- coding: utf-8 -*-
"""
Created on Tue Feb 28 10:21:28 2017

@author: user 1
"""

from operator import itemgetter

"""return a list of tuples of water level that exceeds tolerence level""" 

def stations_level_over_threshold(stations, tol):
    
    stations_ex_tol = [] 
    """calling the function of relative water level in station.py"""

    for station in stations:
        relative_water_level = station.relative_water_level()
            
        if relative_water_level != None: 
            if relative_water_level > tol:
                stations_ex_tol.append((station, relative_water_level))
                
            else:
                pass

    stations_over_threshold = sorted (stations_ex_tol, key=itemgetter(1), reverse = True)
    
    return stations_over_threshold

def stations_highest_rel_level(stations, N):
    """returns a list of the N stations at which the water level, 
    relative to the typical range, is highest. In descending order"""
    
    N_stations = [] 

    for station in stations:
        relative_water_level = station.relative_water_level()
        
        if relative_water_level is not None:
            N_stations.append((station, relative_water_level))
            
    """Sorting the stations"""
    
    stations_sorted = sorted (N_stations, key=itemgetter(1), reverse = True)
    
    first_N_highest_stations = stations_sorted[:N]
    
    return first_N_highest_stations
        
        
    